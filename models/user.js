/** 
*  Customer model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Rohit Saidugari <S534735@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true , unique:true},
  firstname:{type:string, required:true},
  lastname:{type:string,required :true},
  mobilenumber:{type:number, required:true,unique:true},
  email: { type: String, required: true, unique: true },
  street1: { type: String, required: true, default: '' },
  street2: { type: String, required: false, default: '' },
  city: { type: String, required: true, default: 'Maryville' },
  state: { type: String, required: true, default: 'MO' },
  zip: { type: String, required: true, default: '64468' },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('User', UserSchema)
